from kubernetes import client, config
import os
from opensearchpy import OpenSearch
import json
import datetime

if os.getenv("DISABLE_KUBE_SSL_VERIFY") == "true":
    import urllib3
    urllib3.disable_warnings()
if os.getenv("OUTPUT_STDOUT") is None:
    if os.getenv("OPENSEARCH_URL") is None:
        print("OPENSEARCH_URL is not set")
        exit(1)

    if os.getenv("OPENSEARCH_INDEX_NAME") is None:
        print("OPENSEARCH_INDEX_NAME is not set")
        exit(1)

    if os.getenv("OPENSEARCH_PASSWORD") is None:
        print("OPENSEARCH_PASSWORD is not set")
        exit(1)

    if os.getenv("OPENSEARCH_USER") is None:
        print("OPENSEARCH_USER is not set")
        exit(1)

# Configs can be set in Configuration class directly or using helper utility
try:
    config.load_incluster_config()
except Exception:
    config.load_kube_config()

v1 = client.CoreV1Api()
'''
{
    "image": {
        "namespaces": ["ns1", "ns2"],
        "occurrences": 0,
        "registry": "docker.io"
        "path": "/mysql"
        "tag": 1.2.3
    }
}
'''
images = dict()
namespaces = v1.list_namespace()
ns_list = [ namespace.metadata.name for namespace in namespaces.items ]
date = datetime.datetime.now(datetime.timezone.utc).isoformat()
for namespace in ns_list:
    pods = v1.list_namespaced_pod(namespace).items
    for pod in pods:
        for container in pod.spec.containers:
            image = container.image
            if image in images.keys():
                images[image]['namespaces'].add(namespace)
                images[image]['occurrences'] = images[image]['occurrences']+1
            else:
                # Cover edge case of docker hub
                if "." not in image:
                    image = "registry.hub.docker.com/"+image
                images[image] = {
                            "image": image,
                            # Use set instead of list to avoid repetition
                            "namespaces": set([namespace]),
                            "occurrences": 1,
                            "registry": image.split('/')[0],
                            "path": "/".join(image.split(':')[0].split('/')[1:]),
                            "tag": image.split(":")[1],
                            "@timestamp": date
                        }
for image in images.keys():
    # Replace sets with lists
    images[image]['namespaces'] = list(images[image]['namespaces'])

# Override output to stdout
if os.getenv("OUTPUT_STDOUT") == "json":
    print(json.dumps(images, indent=4))
    exit(0)
if os.getenv("OUTPUT_STDOUT") == "text":
    import prettytable
    table = prettytable.PrettyTable()
    table.field_names = ["Image", "Occurrences", "Namespaces"]
    table.sortby = "Occurrences"
    table.reversesort = True
    for image in images.keys():
        table.add_row([images[image]['image'], images[image]['occurrences'], images[image]['namespaces']])
    print(table)
    exit(0)

# Upload to opensearch
client = OpenSearch(
    hosts = [ os.getenv("OPENSEARCH_URL") ],
    http_compress = True, # enables gzip compression for request bodies
    http_auth = (os.getenv("OPENSEARCH_USER"), os.getenv("OPENSEARCH_PASSWORD")),
    use_ssl = True ,
    verify_certs = True if os.getenv("DISABLE_OPENSEARCH_SSL_VERIFY") is None else False,
    ssl_assert_hostname = True if os.getenv("DISABLE_OPENSEARCH_SSL_VERIFY") is None else False,
    ssl_show_warn = False,
)
# Build bulk body
body = ""
for image in images.keys():
    data=json.dumps(images[image])
    operation=json.dumps({"create": { "_index": os.getenv("OPENSEARCH_INDEX_NAME")}})
    body=body+operation+"\n"+data+"\n"

response = client.bulk(body=body, index=os.getenv("OPENSEARCH_INDEX_NAME"))
if response['errors']:
    for item in response['items']:
        print(json.dumps(item))
    print("Exiting with errors")
    exit(1)
print("Exiting without errors")
