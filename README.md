# Image monitor
This project scans a kubernetes cluster's images and saves the results in opensearch
![Opensearch Dashboards](/.media/opensearch-dashboards.png "Opensearch Dashboards")

# Usage
## Local
Use locally with kubectl configuration, set the "OUTPUT_STDOUT" variable and parse the json output with jq
```
export OUTPUT_STDOUT=json
python3 main.py | jq -r 'keys[] as $k | "\(.[$k] | .occurrences) \($k).image"' | sort -r
```
```
5 registry.home.lan/ubuntu:focal.image
3 registry.home.lan/rclone/rclone:1.image
3 registry.home.lan/metallb/speaker:v0.9.3.image
```
or view the data as a table
```
export OUTPUT_STDOUT=text
python3 main.py
```
```
+-----------------------------------------------------------------+-------------+------------------------+
|                              Image                              | Occurrences |       Namespaces       |
+-----------------------------------------------------------------+-------------+------------------------+
|                  registry.home.lan/ubuntu:focal                 |      5      |       ['rclone']       |
|                registry.home.lan/rclone/rclone:1                |      3      |       ['rclone']       |
|             registry.home.lan/metallb/speaker:v0.9.3            |      3      |   ['metallb-system']   |
```
## Kubernetes
This is the recommended way for long-term monitoring. Configure your opensearch cluster and deploy as a cronjob using the kubernetes example.

# Environment Variables
## DISABLE_KUBE_SSL_VERIFY
Set to true to disable SSL verification for kubernetes

## DISABLE_OPENSEARCH_SSL_VERIFY
Set to true to disable SSL verification for opensearch

## OPENSEARCH_URL
Set to the opensearch URL in the form of https://opensearch.domain.local:9200

## OPENSEARCH_INDEX_NAME
Set to the opensearch index name

## OPENSEARCH_USER
Set to the opensearch user

## OPENSEARCH_PASSWORD
Set to the opensearch password

## OUTPUT_STDOUT
- Set to "json" for a complete json output
- Set to "text" for a simple text output